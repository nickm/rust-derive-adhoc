[package]
name = "derive-deftly-tests"
version = "0.0.1"
edition = "2021"
license="MIT"
authors=["Ian Jackson <ijackson@chiark.greenend.org.uk>",
         "and the contributors to Rust derive-deftly"]
description="An ergonomic way to write derive() macros"
homepage = "https://gitlab.torproject.org/Diziet/rust-derive-deftly"
repository = "https://gitlab.torproject.org/Diziet/rust-derive-deftly"
publish = false

[features]
ui = []
recent = [] # We're not running with the MWRV compiler
default = []
full = ["derive-deftly/full"]
# We can't make macrotest, ui, recent and full default features because
# in 1.54 (which is why we want this) --no-default-features doesn't work.

# *Some* of the cargo features from derive-deftly/macros
# need to be reproduced here.  We don't need their actual consequences,
# since the way we invoke the tests gets those via the dependency
# on derive-deftly/full, above.
case = []
meta-as-items = []
#
# `expect` is not here because it doesn't work without a build.rs,
# and anyway we don't test that functionality here.

[dependencies]
derive-deftly = { path = "..", version = "*", default-features = false, features = ["minimal-1"] }
easy-ext = "1"
# educe 0.5.0 to 0.5.10 inclusive are broken but cargo cannot express this
#  https://github.com/magiclen/educe/issues/14
#  https://github.com/magiclen/educe/pull/15
educe = ">=0.4.6, <0.6"
glob = "0.3"
paste = "1"
static_assertions = "1"
toml = ">=0.5.0, <0.9"

# Really, this should be regex 1.8 but only if feature "recent" is enabled.
# Some of our tests in directly.rs and sub-modules require regex 1.8.
# However, regex 1.8 is not compatuble with our MSRV.
# This would be fine, since those tests don't run withotu feature "recent".
# But!  cargo can't do feature-dependent resolution.  If we write this
# dependency sa optional, 1.8, the Cargo.lock.minimal ends up using 1.8
# too - and there's no way to express that that's not what we want.
# So this version requirement is too loose.
#
# This all works in practice because:
#  - for the minimal versions, the Cargo.lock.minimal chooses an old
#    regex, but we don't actually exercise the code paths that would
#    require new regex.
#  - for the main tests, our Cargo.lock ends up containing a recent
#    regex anyway.  This is true of a fully regenerated lockfile too.
# The alternative would be to use only older regex features everywhere,
# or to have multiple lockfiles.
#
# If you do too much messing about with dependencies, the Cargo.lock
# can end up in a state where we run these tests with regex < 1.8
# and then it breaks.  See
#   https://gitlab.torproject.org/Diziet/rust-derive-deftly/-/issues/85
regex = "1"

indexmap = ">=1.8, <3"
itertools = ">=0.10.1, <0.13"
proc-macro-crate = ">=1.1.3, <4"
proc-macro2 = "1.0.53"
quote = "1"
sha3 = "0.10"
strum = { version = ">=0.24, <0.27", features = ["derive"] }
syn = { version = "2.0.53", features = ["extra-traits", "full"] }
void = "1"

heck = ">=0.4, <0.6"

macrotest = { version = "1.0.10", optional = true }
trybuild = "1.0.46"

[lib]
path = "tests.rs"

[[test]]
name = "macrotest"
path = "macrotest.rs"

[[test]]
name = "trybuild"
path = "trybuild.rs"

[[test]]
name = "stderr"
path = "stderr.rs"
