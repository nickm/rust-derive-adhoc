# Changelog, MSRV policy and cargo features

## cargo features

Features are provided to allow for a build with reduced dependencies.

 * `full`: Metafeature.
   Enable all reasonable, non-experimental, features.
 * `case`: [Case conversions](../doc_reference/index.html#case-changing),
   using `heck`.
 * `expect`: the [`expect`](../doc_reference/index.html#expect-items-expect-expr--syntax-check-the-expansion)
   expansion option for syntax checking.
 * `meta-as-items`: handling attributes containing (syntax-checked) items via
   [`${Xmeta(...) as items}`](../doc_reference/index.html#tmeta-vmeta-fmeta--deftly-attributes)
 * `minimal-1`: Minimal feature set.  Must be enabled.

All of the above features are enabled by default.

## MSRV and MSRV policy

The Minimum Supported Rust Version for derive-deftly is 1.56.

We expect to increase it cautiously and only with good reason.
(However, MSRV increase would be a minor version bump.)

## Changelog

### 0.11.0

#### Breaking

 * `${Xmeta as ...}` no longer has a default.
   Change to `as expr`, `as ty`, etc.
 * `${Xmeta as tokens}` is now `as token_stream`
   (and we now document its inherent precedence hazards).
 * `${Xmeta as ty}` and `$ftype` insert `::` before generic arguments
   (ie, they normalise the turbofish);
   this is generally breaking only with `${approx_equal ..}`.
 * `None`-delimited groups are ignored by `${approx_equal ..}`.
 * Meta attributes `#[deftly]` must actually be used in
   an expanded `$tmeta`/`$vmeta`/`$fmeta` expansion,
   or (as applicable),
   tested with a `tmeta`/`vmeta`/`fmeta` condition.
   I.e. meta attribute use checking is dynamic:
   merely mentioning the meta somewhere in an unused part of the template
   is not enough.

#### Added

 * `${Xmeta as expr/ident/path/items}`
 * `meta-as-items` cargo feature (for `${Xmeta as items}`),
   part of `full` and enabled by default.
 * Parenthesised type path attributes, processed by `${Xmeta as ty}`,
   can now be pasted with `${paste }`.

#### Improved

 * `${Xmeta as ty}` uses a `None`-delimited `Group`
   (although, this is not effective with current compilers:
   [rust-lang/rust#67062](https://github.com/rust-lang/rust/issues/67062).
 * Precedence considerations with `${define }` and `${Xmeta }`
   are documented in the reference.
 * Short-circuiting behaviour of `any` and `all` conditions is documented.
 * Improved some error messages.

#### pub template semver

Updating to derive-deftly 0.11.0 is a breaking change
to the API of a crate which exports templates:

The scheme for collating information about meta attributes,
across multiple template invocations, has been completely changed.
So the calling convention for the template macro is quite incompatible.

### 0.10.5

#### Breaking (minor)

 * Fixed `$vdefbody` to always include precisely a comma after enum
   variants.  The previous wrong behaviour was shown in the example output,
   but wasn't usable in the documented/intended manner.  \#63, !325.

#### Improved

 * Improved some error messages.
 * Docs clarifications and fixes.
 * Testing fixes and robustness improvements.

### 0.10.4

#### Added

 * `${ignore }` keyword.

#### Fixed

 * Avoid spurious "unexpected token" errors after other errors.

#### Improved

 * Documentation updates.
 * Slightly improved an error message.

### 0.10.3

#### Fixed

 * Fix `approx_equal` not to treat its arguments as equal
   if one of them was a prefix of the other.  (#52)

#### Breaking (minor)

 * Reject `expect expr` early, in `define_derive_deftly!`.
   (Such a template could never be successfully used.)

#### Added

 * `tgens` condition: true if there are any generics.
 * `${dbg ..}` debug dump expansion, and `dbg(...)` debug dump condition.
 * `is_empty` condition for testing an expansion for emptiness.

#### Improved

 * Improvements to reference docs.

### 0.10.2

#### Fixed

 * Docs references to `pub_template_semver_check` corrected
   (including in changelog entry for 0.10.0).

#### Improved

 * Use the 2021 edition.  Some minimum dependency requirements
   tightened, but: no MSRV impact.

### 0.10.1

#### Added

 * `${error "message"}` keyword for explicitly causing compile errors.

#### Improved

 * Prettier output from `${dbg_all_keywords}`.
 * Better error messages for unecognised `#[deftly(...)]` attributes.

### 0.10.0

#### Breaking

 * Reject invalid templates in `define_derive_deftly!`,
   even if they are never invoked.
 * Reject `#[deftly(...)]` attributes not recognised by
   any relevant template.

#### Added

 * `pub_template_semver_check!` for helping ensure semver
   compliance when exporting macros, and associated docs.

#### Fixed

 * Fixed a largely-theoretical concurrency bug which could cause
   wrong error output if multiple invocations generated identical,
   but syntactically-invalid, expansions.  (!262 / !263).
 * Improve spans in certain error messages relating to `Xmeta`.
 * Fix heading formatting issues in the entry for 0.9.0.

#### pub template semver

Updating to derive-deftly 0.10.0 is a breaking change
to the API of a crate which exports templates:

Exported templates are not compatible due to renaming of the
internal keyword `$_da_intern_crate` to `$_dd_intern_crate`.

### 0.9.2

#### Improved

 * docs: Fixed the README.md cross-reference and stability information.

### 0.9.1

#### Improved

 * Documentation and error message fixes, tidying up after renames.
 * Other minor documentation improvements.

### 0.9.0

#### Breaking

 * Crate renamed to `derive-deftly`, and many other renamings:
 * `#[derive_deftly_adhoc]` is now required for `derive_deftly_adhoc!`
   (ie, non-precanned templates).
 * To export a driver, `#[derive_deftly_adhoc(pub)]` is now needed,
   rather than `#[derive_adhoc(pub)]`.

```text
Old name (0.8.x and earlier)	New name (0.9.x and later)

define_derive_adhoc!            define_derive_deftly!

#[derive(Adhoc)]                #[derive(Deftly)]
#[derive_adhoc(Template)]       #[derive_deftly(Template)]
#[adhoc(...)]                   #[deftly(...)]

derive_adhoc_driver!            derive_deftly_driver!
derive_adhoc_template!          derive_deftly_template!
derive_adhoc_expand!            derive_deftly_engine!

derive_adhoc!                   derive_deftly_adhoc!
```

##### pub template semver

Updating to `derive-deftly` from `derive-adhoc` is a breaking change
to the API of a crate which exports templates.

### 0.8.1

#### Fixed

 * Fix doubled dollar deescaping in precanned d-a macros.
   Fixes `macro_rules! .. { { $$m:expr .. } ... }`.

#### Improved

 * Improvements to macro rustdoc documentation.
 * Minor internal improvements.

### 0.8.0

#### Nominally breaking (pub template semver)

**We recommend you do *not* treat these changes as semver-breaking
for template-defining crates.**

 * Reject misplaced `#[derive_adhoc]` attributes.
 * Reject malformed attributes like `#[adhoc("42")]`
   and `#[adhoc(std::cell::<String>)]`.

Technically these are breaking changes, since users of exported
templates might have provided these wrong attributes, which were
previously ignored and are now properly rejected.  The attributes are
interpreted by the version of derive-adhoc which defines the template.
However, the likelihood of this breaking a working build seems low.

#### MSRV

 * MSRV increased from 1.54 to 1.56.
   Principally because we need this to update to syn 2.
   1.56 is the first release with Rust 2021, but
   we're not updating to the 2021 edition yet.

#### Fixed

 * Fixed a spurious clippy lint `crate_in_macro_def` (#27).
 * More reasonable handling of `#[adhoc(..)]` attributes whose meta
   information has paths some of which are nested sub-paths of others.
 * Work around a strange bug with literals and spans which can cause
   a `compiler/fallback mismatch` panic during error reporting
   with some compiler versions. (!182)

#### Improved

 * Docuemntation improvements (notably, many cross-references added
   from the tutorial to the reference).
 * Dependency updates (notably, we're now using syn 2).
 * Improvements to tests, CI, `HACKING.md`, Cargo lockfile handling.
 * Lockfile (`Cargo.lock`) is now committed to git under that name.

### 0.7.3

#### Added

 * `approx_equal` condition, for testing a form of token stream equivalence.

#### Fixed

 * docs: Corrected some internal links to refer to the correct anchors.

#### Improved

 * `$ttype`, `$tdeftype` and `$vtype` no longer include `::<>` or `<>`
   unless the original toplevel driver definition did.

### 0.7.2

#### Added

 * `${Xmeta(...)}` within pasting now defaults to `... as str`,
   so that doesn't need to be specified each time.

### 0.7.1

#### Fixed

 * Fix reference documentation examples for `$Xattr`.

### 0.7.0

#### Breaking

 * Reject inner attributes (`#![...]` and `//!...`) anywhere in templates:
   these are now reserve for future expansion.
   To resolve: use inner attributes instead.

#### Added

 * New `${define }` and `${defcond }` facility for reuseable template
   fragments and conditions, to save repetition in templates.  (#14)
 * Abbreviated `$<...>` syntax for `${paste ...}`.  (#23)

#### Fixed

 * Allow nested pasting when intermediate pastes aren't valid idents.
 * Fixed bugs relating to pasting of keywords and raw identifiers.
 * docs: Fixed minor bugs in the reference.

### 0.6.1

#### Fixed

 * Fixed a few broken docs links and similar infelicities.

#### Improved

 * Change dollar-escaping pseudo-keyword to `$orig_dollar`.
   (Internal change; should not have any user-visible effect.)

### 0.6.0

#### Breaking

 * `$fvis` and `fvis` now refer to the top-level visibility for enums.
   (The previous behaviour is now available from `$fdefvis`/`fdefvis`.)
 * `fmeta` and `vmeta` now fail when used outside a field or variant,
   rather than searching through the whole item.
   (`$fmeta` and `$vmeta` didn't search and are unchanged.)
 * Actually make structs and unions be treated as having one "variant".
 * `$tdefgens` and `$tgens` include trailing comma when nonempty,
   as documented and intended.

#### Added

 * `define_derive_adhoc!` supports doc comments.
 * `$fdefvis`/`fdefvis` for the textual visibility of a field.

#### Fixed

 * Reference doc example snippets: many errors corrected.
 * Added missing cargo dep on `syn/extra-traits`.

#### Improved

 * Trailing comma no longer added inside generics in `$ttype` `$vtype`.
 * Relaxed upper dependency bounds for `strum`, `itertools`.
 * docs: Reference: example snippets: now tested, and many added.
 * docs: Introduction: now has a Table of Contents.

### 0.5.0

#### Breaking

 * Case changing: non-snake-case keywords for case change instructions
   abolished (to make room for possible future reservation of
   non-snake-case keywords as user-defined ones).  Change the case
   of the keyword to snake case.

#### Improved

 * Documentation: tidying and a few more examples.

### 0.4.0

#### Breaking

 * `${paste }` no longer allows non-string literals in its content.
 * Invalid literal content within `${paste }` is now rejected
   during parsing, so even if that part of the template isn't expanded.
 * `${Xmeta... as lit}` is abolished, in favour of `... as str`
   (which has more consistent semantics).
 * `${Xmeta}` without `as` is no longer allowed in `${paste ...}`.
 * In `#[adhoc(attr="VALUE")]`, now only *string* literals are allowed.

#### Added

 * `${paste }` and `${CASE_CHANGE ...}` can now be nested,
    either (or both) ways round.
 * `${Xmeta... as str}`, `${Xmeta... as tokens}`.

#### Improved

 * Better error messages when `${paste }` produces a bad identifier.
 * docs: Minor improvements to reference.
 * internal: CI tests improved and extended
 * internal: cleanups, and internal docs improved.

### 0.3.0

#### Breaking

 * cargo features introduced.
   Currently, all enabled by default -
   no breakage if default features enabled.
   - Case conversion (and `heck` dependency) now behind `case`.
   - `minimal-1` introduced; it must be enabled.

#### Added

 * Expansion options facility
 * `expect items`, `expect expr` option, for improved errors
   when template expands to invalid Rust syntax.
   (`expect` cargo feature.)
 * `for struct`, `for enum`, `for union` option,
   for improved errors due to misue of a template.
 * `dbg` option, for dumping template expansions to compiler stderr.
 * `$dbg_all_keywords` keyword, for dumping keyword expansions.
 * `full` and `minimal-1` cargo meta features.

#### Improved

 * docs: Much expanded and improved tutorial (still a work in progress).
 * docs: Various corrections to reference docs.
 * docs: Reference documentation retitled and module renamed.
 * error handling: Better messages from certain situations involving
   multiple (incompatible) derive-adhoc versions.
 * tests: Made less fragile (more pinning of test dependencies).
 * tests: Improved CI checks on documentation, TODOs, etc.
 * internal: new HACKING.md file for helping develop derive-adhoc.

### 0.2.2

#### Fixed (future compatibility)

 * Pinned dependency from `derive-adhoc` to `derive-adhoc-macros`.
 * Handling of certain supposedly-future-compatible options fixed.
   ("future driver options" argument to `d_a_t_T`).

#### Improved

 * Better error messages with usupported combinations of features
   with mixed derive-adhoc versions.  #10.
 * Compatibility with derive-adhoc 0.2.0 tested in CI.

### 0.2.1

#### Fixed

 * `$vpat` expansion includes necessary post-field comma.  #15.
 * Docs typo fixes.

#### Improved

 * docs: Much expanded tutorial (still a work in progress)

### 0.2.0

#### Breaking

 * `$tgens` no longer includes defaults for generics.
 * `$Xattrs` by default
   outputs all attributes except derive-adhoc ones,
   rather than nothing (breaking bugfix).
 * `$vmeta` for a struct (not enum) processes top-level attributes,
   rather than imagining that there are no variant/value attributes.
 * Fixed hygiene (span) for `${paste }`;
   now it's consistently that of the template
   (but disagrees with the hygiene span of `$fname`).

#### Added

 * `$fpatname` `$vpat` `$vtype`, for value matching and construction
 * `$fvis` `$tvis`, for visibility (also as booleans)
 * `is_struct` `is_union`
   `v_is_unit` `v_is_tuple` `v_is_named`,
   conditions for driver shape.
 * `$tdefkwd` `$tdeftype` `$tdefvariants` `$vdefbody`
   `$fdefine` `$tdefgens`,
   for defining derived types,
 * `$select1`, exactly-one conditional
 * Support exporting a template to other crates,
   and `$crate` expansion for referring to template crate items.
 * Support exporting a driver to other crates
   (rather hazardous).

#### Fixed

 * Do not claim that `$ttype` includes any leading path elements.
 * `$` in templates always has proper span for error reporting
 * `$` is properly escaped in drivers
 * `$Xmeta` can expand to arbitrary tokens from an attribute value

#### Improved

 * docs: New tutorial (still a work in progress)
 * docs: Template syntax reference overhauled
 * Many other minor improvements
 * New tests/examples

### 0.1.0

 * First publicly advertised release.
   Much important documentation and
   many important features still missing.

### 0.0.1

 * Initial release to crates.io, not widely advertised.
